﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveData
{
    public int From;
    public int To;

    public MoveData(int from, int to)
    {
        From = from;
        To = to;
    }
}

public class HanoyLogic
{
    private int _ringsCount;

    private List<MoveData> _moves;
    
    public HanoyLogic(int ringsCount)
    {
        _ringsCount = ringsCount;
    }

    public List<MoveData> GetMoves()
    {
        _moves = new List<MoveData>();
        
        HanoiRecursive(_ringsCount, 1, 3, 2);
        
        return _moves;
    }

    private void HanoiRecursive(int count, int from, int to, int buf)
    {								
        if (count != 0)
        {
            HanoiRecursive(count - 1, from, buf, to);

            _moves.Add(new MoveData(from, to));

            HanoiRecursive(count - 1, count, to, from);
        }
    }
    
}
