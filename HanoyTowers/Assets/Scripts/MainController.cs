﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MainController : MonoBehaviour
{
	public Transform Pile;
	public GameObject DiscPrefab;

	public float MinDiscSize = 0.4f;
	public float MaxDiscSize = 2f;
	public float MaxHeight = 2f;
	
	// Use this for initialization
	void Start ()
	{
		int count = 6;
		var logic = new HanoyLogic(count);
		var moves = logic.GetMoves();
		Debug.Log(moves);

		var discHeight = MaxHeight / count;

		var pileVector = Pile.localPosition;
		
		for (int i = 1; i <= count; i++)
		{
			var disc = Instantiate(DiscPrefab);
			var size = Mathf.Lerp(MinDiscSize, MaxDiscSize, (1f / count * (i)));

			pileVector.y = MaxHeight - i * discHeight;
			
			disc.transform.localPosition = pileVector;
			disc.transform.localScale = new Vector3(size, discHeight, size);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
